provider "aws" {
  region = "eu-west-1"
}

module "myvpc" {
  source = "../.."

  organization = "test-example"
  name = "test-example-vpc"
}

