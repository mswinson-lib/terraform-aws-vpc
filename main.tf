resource "aws_vpc" "current" {
  cidr_block = "10.0.0.0/16"
  enable_dns_hostnames = true

  tags {
    Name = "${var.name}"
    Organization = "${var.organization}"
  }
}

resource "aws_subnet" "public" {
  vpc_id = "${aws_vpc.current.id}"
  cidr_block = "10.0.0.0/24"
  map_public_ip_on_launch = true
    
  tags {
    Name = "${var.name}-public"
    Organization = "${var.organization}"
  }
}

resource "aws_internet_gateway" "current" {
  vpc_id = "${aws_vpc.current.id}"
  
  tags {
    Name = "${var.name}-igw"
    Organization = "${var.organization}"
  }
}

resource "aws_eip" "current" {
  vpc = true
}

resource "aws_nat_gateway" "public-nat" {
  allocation_id = "${aws_eip.current.id}"
  subnet_id = "${aws_subnet.public.id}"
}

resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.current.id}"
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = "${aws_internet_gateway.current.id}"
  }

  tags {
    Organization = "${var.organization}"
  }
}

resource "aws_route_table_association" "public" {
  subnet_id = "${aws_subnet.public.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_main_route_table_association" "public" {
  vpc_id = "${aws_vpc.current.id}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_security_group" "admin" {
  name = "${var.name}-admin"
  description = "admin access"

  vpc_id = "${aws_vpc.current.id}"

  ingress {
    from_port = "2224"
    to_port = "2224"
    protocol = "tcp"
    cidr_blocks = [ "0.0.0.0/0" ]
  }

  tags {
    Name = "${var.name}-admin"
    Organization = "${var.organization}"
  }
}
