variable "organization" {
  type = "string"
  default = "myorg"
}

variable "name" {
  type = "string"
  default = "myvpc"
}

