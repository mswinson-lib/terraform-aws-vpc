# terraform-aws-vpc

[Terraform Module]() for AWS VPC

## Features


## Usage

```HCL
module "vpc" {
  source = "bitbucket.org/mswinson-lib/terraform-aws-vpc"

  organization = "myorg"   # default
  name = "myvpc"           # default
}
```

## Inputs

| Name | Description | Type | Default | Required |
| - | - | - | - | - |
| organization | | string | myorg | true |
| name | | string | myvpc | true |


## Outputs

| Name | Description |
| - | - |
| vpc | vpc id |
| igw | internet gateway id |

