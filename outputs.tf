output "vpc" {
  value = "${aws_vpc.current.id}"
}

output "igw" {
  value = "${aws_internet_gateway.current.id}"
}
